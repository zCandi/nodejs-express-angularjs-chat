var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');
 
server.listen(8090);

// io.on('connection', function (socket) {
 
//   console.log("new client connected");
//   var redisClient = redis.createClient();
//   redisClient.subscribe('message');
 
//   redisClient.on("message", function(channel, message) {
//     console.log("mew message in queue "+ message + "channel");
//     socket.emit(channel, message);
//   });
 
//   socket.on('disconnect', function() {
//     redisClient.quit();
//   });
 
// });


// var express = require("express");
// var app = express();
// var port = 3701;

//Template
// app.set('views', __dirname + '/public');
// app.engine('html', require('ejs').renderFile);
// app.set('view engine', 'html');
// app.get("/", function(req, res){
//     res.render("index");
// });

// app.get("/", function(req, res){
//     res.send("It works!");
// });

//Socket
//app.use(express.static(__dirname + '/public'));
//var io = require('socket.io').listen(app.listen(port));
io.on('connection', function (socket) {
    socket.emit('message', { message: 'welcome to the chat' });
    socket.on('send', function (data) {
        io.sockets.emit('message', data);
    });

    socket.on('typing', function (data) {
        io.sockets.emit('typing', data);
    });

    socket.on('blur', function (data) {
        io.sockets.emit('blur', data);
    });
});